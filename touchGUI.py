from tkinter import font
import tkinter as tk
import urllib.request

HOST = "localhost"
BARTENDRURL = "http://"+HOST+":8080/?action="
ACCESSPOINTMGRURL = "http://"+HOST+":80/?action="
JETTYPOLLDELAY = 1000
BUTTONCOLOUR = "#abd4eb"


def home(*args):
	urllib.request.urlopen(BARTENDRURL+"home").read()

def stop(*args):
	urllib.request.urlopen(BARTENDRURL+"stop").read()

def up(*args):
	urllib.request.urlopen(BARTENDRURL+"autoup").read()

def down(*args):
	urllib.request.urlopen(BARTENDRURL+"down").read()

def auto(*args):
	urllib.request.urlopen(BARTENDRURL+"autotempon").read()
	
def off(*args):
	urllib.request.urlopen(BARTENDRURL+"alloff").read()

def shutdownrpi(*args):
	urllib.request.urlopen(BARTENDRURL+"shutdownhnow").read()
	
def polljetty():

	try:
		wifiinfo = urllib.request.urlopen(ACCESSPOINTMGRURL+"currentwifi").read().split()
		ssid['text'] = wifiinfo[0]
		ipaddress['text'] = wifiinfo[1]
		
		ethinfo = urllib.request.urlopen(ACCESSPOINTMGRURL+"currentethernet").read().split()
		ethaddress['text'] = ethinfo[0]	
	except:
		pass

	try:
		deviceoutput = urllib.request.urlopen(BARTENDRURL+"deviceoutput").read()
		s = deviceoutput.split()
		if float(s[1])==-1:
			presspos['text'] = "unknown"
		else:
			presspos['text'] = s[1].decode("utf-8") + " mm"
			
		currentforce['text'] =  s[2].decode("utf-8") + "kg"
		targetforce['text'] =  s[3].decode("utf-8") + "kg"
			
		temp['text'] =  s[5].decode("utf-8") + u"\N{DEGREE SIGN}" + "C"
		tempsetpoint['text'] = s[10].decode("utf-8") + u"\N{DEGREE SIGN}" + "C"
			
		heaters = "unknown"
		if s[7].decode("utf-8") == "ON":
			heaters = "ON"
		if s[7].decode("utf-8") == "OFF":
			heaters = "OFF"
		if s[11].decode("utf-8") == "YES":
			heaters = "AUTO HEATING"
		heaterstatus['text'] =  heaters
	except:
		pass
	
	root.after(JETTYPOLLDELAY, polljetty)

	
root = tk.Tk()
root.attributes('-fullscreen', True)
root.title("bartendr")
root.geometry("320x480")

TXTLEFT = 20

ypos=0

ypos += 10

# press info
tk.Label(text="press position:", font=("Helvetica", 12)).place(x=TXTLEFT, y=ypos)
presspos = tk.Label(text="unknown", font=("Helvetica", 12))
presspos.place(x=150, y=ypos)

ypos += 25

tk.Label(text="current force:", font=("Helvetica", 12)).place(x=TXTLEFT, y=ypos)
currentforce = tk.Label(text="unknown", font=("Helvetica", 12))
currentforce.place(x=150, y=ypos)

ypos += 25

tk.Label(text="target force:", font=("Helvetica", 12)).place(x=TXTLEFT, y=ypos)
targetforce = tk.Label(text="unknown", font=("Helvetica", 12))
targetforce.place(x=150, y=ypos)

ypos += 25

# press controls
tk.Button(text="HOME", bg=BUTTONCOLOUR, command=home, height=3, width=15, activebackground=BUTTONCOLOUR).place(x=10, y=ypos)
tk.Button(text="AUTO-UP", bg=BUTTONCOLOUR, command=up, height=3, width=15, activebackground=BUTTONCOLOUR).place(x=160, y=ypos)

ypos += 70

tk.Button(text="STOP", bg=BUTTONCOLOUR, command=stop, height=3, width=15, activebackground=BUTTONCOLOUR).place(x=10, y=ypos)
tk.Button(text="DOWN", bg=BUTTONCOLOUR, command=down, height=3, width=15, activebackground=BUTTONCOLOUR).place(x=160, y=ypos)

ypos += 90

# oven info
tk.Label(text="temperature:", font=("Helvetica", 12)).place(x=TXTLEFT, y=ypos)
temp = tk.Label(text="unknown", font=("Helvetica", 18))
temp.place(x=150, y=ypos-6)

ypos += 25

tk.Label(text="setpoint:", font=("Helvetica", 12)).place(x=TXTLEFT, y=ypos)
tempsetpoint = tk.Label(text="unknown", font=("Helvetica", 12))
tempsetpoint.place(x=150, y=ypos)

ypos += 25

tk.Label(text="heaters:", font=("Helvetica", 12)).place(x=TXTLEFT, y=ypos)
heaterstatus = tk.Label(text="unknown", font=("Helvetica", 12))
heaterstatus.place(x=150, y=ypos)

ypos += 25

# oven controls
tk.Button(text="AUTO ON", bg=BUTTONCOLOUR, command=auto, height=3, width=15, activebackground=BUTTONCOLOUR).place(x=10, y=ypos)
tk.Button(text="OFF", bg=BUTTONCOLOUR, command=off, height=3, width=15, activebackground=BUTTONCOLOUR).place(x=160, y=ypos)

ypos += 80

# network info
tk.Label(text="wifi ssid:", font=("Helvetica", 12)).place(x=TXTLEFT, y=ypos)
ssid = tk.Label(text="unknown", font=("Helvetica", 12))
ssid.place(x=110, y=ypos)

ypos += 25

tk.Label(text="wifi IP:", font=("Helvetica", 12)).place(x=TXTLEFT, y=ypos)
ipaddress = tk.Label(text="unknown", font=("Helvetica", 12))
ipaddress.place(x=110, y=ypos)

# shutdown button
tk.Button(text="shut\ndown", bg=BUTTONCOLOUR, command=shutdownrpi, height=2, width=6, activebackground=BUTTONCOLOUR).place(x=230, y=ypos)

ypos += 25

tk.Label(text="ethernet IP:", font=("Helvetica", 12)).place(x=TXTLEFT, y=ypos)
ethaddress = tk.Label(text="unknown", font=("Helvetica", 12))
ethaddress.place(x=110, y=ypos)




root.after(JETTYPOLLDELAY, polljetty)
root.mainloop()


