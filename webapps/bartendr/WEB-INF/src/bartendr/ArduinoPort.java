package bartendr;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import java.util.ArrayList;
import java.util.List;

public class ArduinoPort implements SerialPortEventListener {

    private byte[] buffer = new byte[256];
    private static SerialPort serialPort = null;
    public boolean connected = false;
    private int buffSize = -1;
    private String portname = "/dev/ttyUSB0";
    private static final int DEVICERESETDELAY = 2000;
    private long lastRead = System.currentTimeMillis();
    private long lastReset = System.currentTimeMillis();
    private static final long PERIODICRESET = 3600000; // 1 hour, avoid microcrontroller micros() rollover
    private static final long WATCHDOGDELAY = 10000;
    private static final long DEAD_TIME_OUT = 30000;
    private boolean resetting = false;

    private AppServlet app;
    public static final String UNKNOWN = "<span style=\"color: grey\">unknown</span>";

    public String actuatorDirection;
    public String currentForce;
    public String forceTarget;
    public String actuatorPos;
    public String ambientTemp;
    public String ovenTemp;
    public String ovenSetpoint;
    public String tempControlActive;
    public String barTemp;
    public String convectfan;
    public String topheaters;
    public String backheaters;
    public String casefan;
    public String positiontarget;

    private static final String ENABLED = "<b>ENABLED</b>";
    private static final String DISABLED = "disabled";

    public static final double BARSQIN = 6.75;
    private volatile boolean commandlock = false;
    private volatile List<Byte> commandList = new ArrayList<>();
    public String deviceoutput = "";

    // device commands
    public static final byte ZERO = 't';
    public static final byte SETPOINT = 'j';
    public static final byte FORCETARGET = 'g';
    public static final byte UP = '8';
    public static final byte STOP = '0';
    public static final byte DOWN = '7';
    public static final byte HOME = 'h';
    public static final byte GOTOPOSITION = 'P';
    public static final byte UPAUTOLOADHOLD = 'U';
    public static final byte CONVECTFANON = 'm';
    public static final byte CONVECTFANOFF = 'n';
    public static final byte CASEFANON = 'b';
    public static final byte CASEFANOFF = 'c';
    public static final byte TOPHEATERSON = 'p';
    public static final byte TOPHEATERSOFF = 'o';
    public static final byte BACKHEATERESON = 'l';
    public static final byte BACKHEATERSOFF = 'k';
    public static final byte AUTOTEMPON = 'A';
    public static final byte AUTOTEMPOFF = 'B';
    public static final byte ALLOFF = 'x';
    public static final byte PING = 'X';

    private boolean ignorenan = true;
    private int nancount = 0;
    private static final int MAXNAN = 10;

    public ArduinoPort(AppServlet a) {
        app = a;
        new CommandSender().start();
        new WatchDog().start();
        setunknown();
    }

    // periodic device checks
    public class WatchDog extends Thread {

        public WatchDog() { this.setDaemon(true); }

        public void run() {

            while (true) {

                long now = System.currentTimeMillis();

                if (now - lastReset > PERIODICRESET && connected &&
                        !tempControlActive.equals(ENABLED) && !resetting) {
                    app.printlog("periodic device reset");
                    reset();
                }

                if (!connected && !resetting) {
                    app.printlog("attempting device connect");
                    connect();
                }

                if (now - lastRead > DEAD_TIME_OUT && connected  && !resetting) {
                    app.printlog("no response from device, attempting reset");
                    reset();
                }

                if (nancount >= MAXNAN) ignorenan = false;

                sendCommand(PING);

                delay(WATCHDOGDELAY);

            }
        }
    }

    // set initial values
    private void setunknown() {
        actuatorDirection = UNKNOWN;
        currentForce = UNKNOWN;
        forceTarget = UNKNOWN;
        actuatorPos = UNKNOWN;
        ambientTemp = UNKNOWN;
        ovenTemp = UNKNOWN;
        ovenSetpoint = UNKNOWN;
        tempControlActive = UNKNOWN;
        barTemp = UNKNOWN;
        convectfan = UNKNOWN;
        topheaters = UNKNOWN;
        backheaters = UNKNOWN;
        casefan = UNKNOWN;
    }

    // parse incoming serial messages from device
    private void execute(int bufferSize) {
        String output = "";

        for (int i = 0; i < bufferSize; i++)
            output += (char) buffer[i];

        app.printlog(output);

        String s[] = output.split(" ");

        if (s.length == 12) { // 1Hz longer output

            // example device output:  auto-up 15.00 24.69 21.75 20.0 OFF OFF OFF 165.00 NO OFF 0.00
            actuatorDirection = s[0];
            forceTarget = s[1];

            ambientTemp = s[2];
            String tempovenTemp = s[3];
            String tempbarTemp = s[4];
            topheaters = s[5];
            backheaters = s[6];

            convectfan = s[7];
            ovenSetpoint = s[8];
            tempControlActive = s[9];
            casefan = s[10];
            positiontarget = s[11];

            boolean nan = false;

            if (tempovenTemp.equalsIgnoreCase("nan")) {
                if (ignorenan) nancount++;
                else  ovenTemp = tempovenTemp;
                nan = true;
            }
            else  ovenTemp = tempovenTemp;

            if (tempbarTemp.equalsIgnoreCase("nan")) {
                if (ignorenan) nancount++;
                else  barTemp = tempbarTemp;
                nan = true;
            }
            else  barTemp = tempbarTemp;

            if (!nan) {
                nancount = 0;
                ignorenan = true;
            }

            if (tempControlActive.equals("YES")) tempControlActive = ENABLED;
            else if (tempControlActive.equals("NO")) tempControlActive = DISABLED;
        }

        else if (s.length == 2) { // 10Hz position and force output only
            // example device output: 3.40 -0.01

            actuatorPos = s[0];
            currentForce = s[1]; // convertToPSI(s[1])+" psi / "+s[1]+" lbs";
            if (Float.parseFloat(actuatorPos) == -1.0) actuatorPos = UNKNOWN;
        }


        deviceoutput = actuatorDirection+" "+actuatorPos+" "+currentForce+" "+forceTarget+" "+
                ambientTemp+" "+ovenTemp+" "+barTemp+" "+topheaters+" "+backheaters+" "+
                convectfan+" "+ovenSetpoint+" "+tempControlActive+" "+casefan+" "+positiontarget;

    }

    // connect to device
    public boolean connect() {
        if (connected) return false;
        try {
            serialPort = new SerialPort(portname);
            serialPort.openPort();
            serialPort.setParams(115200, 8, 1, 0);
            Thread.sleep(DEVICERESETDELAY);

            serialPort.readBytes(); // clear serial buffer
            app.printlog("connected on port: "+portname);
            serialPort.addEventListener(this, SerialPort.MASK_RXCHAR);//Add SerialPortEventListener
            connected = true;
            lastRead = System.currentTimeMillis();
            return true;
        } catch (Exception e) {e.printStackTrace();  }
        app.printlog("unable to connect to serial port");
        return false;
    }

    // disconnect device
    public void disconnect() {
        if (!connected) return;
        try {
            connected = false;
            app.printlog("disconnecting port: "+portname);
            serialPort.removeEventListener();
            serialPort.closePort();
            setunknown();
        } catch (Exception e) { e.printStackTrace(); }
    }

    // reset device
    public void reset() {
        if (!connected || resetting) return;

        resetting = true;
        app.printlog("resetting device");
        disconnect();

        delay(DEVICERESETDELAY);
        connect();
        lastReset = System.currentTimeMillis();
        resetting = false;

        ignorenan = true;
        nancount = 0;
    }

    // serial input event handler
    public void serialEvent(SerialPortEvent event) {
        if (!event.isRXCHAR())  return;

        try {
            byte[] input = new byte[32];

            if(serialPort == null){
                app.printlog("serial port is null");
                return;
            }

            input = serialPort.readBytes();
            for (int j = 0; j < input.length; j++) {
                if (input[j] == '>') {
                    if (buffSize > 0) {
                        execute(buffSize);
                        buffSize = -1;
                    }
                    buffSize = 0; // reset
                    lastRead = System.currentTimeMillis();

                }else if (input[j] == '<') {  // start of message
                    buffSize = 0;
                } else if (buffSize != -1) {
                    buffer[buffSize++] = input[j];   // buffer until ready to parse
                }
            }

        } catch (SerialPortException e) { e.printStackTrace(); }
    }

    private String celsiusToFahrenheit(String temp) {
        try{
            double c = Double.valueOf(temp);
            double f = c * 1.8 + 32;
            return String.format("%.1f", f);
        }
        catch(NumberFormatException nfe)  { return "<b>ERROR</b>";  }
    }

    private String convertToPSI(String lbs) {
        double psi = Double.valueOf(lbs)/BARSQIN;
//        return String.valueOf(psi);
        return String.format("%.1f", psi);
    }

    // send commands to port
    public void sendCommand(byte[] cmd) {
        if (!connected || resetting) return;

        String text = "sendCommand(): " + (char)cmd[0] + " ";
        for(int i = 1 ; i < cmd.length ; i++)
            text += ((byte)cmd[i] & 0xFF) + " ";   // & 0xFF converts to unsigned byte

        if (cmd[0] != PING) // ignore ping
            app.printlog(text);

        int n = 0;
        while (commandlock) {
            delay(1);
            n++;
        }

        commandlock = true;
        for (byte b : cmd) commandList.add(b);
        commandList.add((byte) 13); // EOL
        commandlock = false;

    }

    public void sendCommand(final byte cmd){
        sendCommand(new byte[]{cmd});
    }

    /** inner class to send commands to port in sequential order */
    public class CommandSender extends Thread {

        public CommandSender() {
            this.setDaemon(true);
        }

        public void run() {

            while (true) {
                if (commandList.size() > 0 &! commandlock) {

                    if (commandList.size() > 15) {
                        commandList.clear();
                        app.printlog("error, command stack up, all dropped");
                        delay(1);
                        continue;
                    }

                    int EOLindex = commandList.indexOf((byte) 13);
                    if (EOLindex == -1) {
                        delay(1);
                        continue;
                    }


                    // in case of multiple EOL chars, read only up to 1st
                    byte c[] = new byte[EOLindex+1];
                    try {
                        for (int i = 0; i <= EOLindex; i++) {
                            c[i] = commandList.get(0);
                            commandList.remove(0);
                        }
                    }  catch (Exception e) {
                        commandList.clear();
                        delay(1);
                        continue;
                    }

                    if (!connected || resetting) continue;

                    try {
                        serialPort.writeBytes(c); // writing as array ensures goes at baud rate?

                    } catch (Exception e) { e.printStackTrace();  }

                }

                delay(1);

            }
        }
    }

    // convenience sleep method
    public void delay(long milliseconds) {
        try { Thread.sleep(milliseconds); }
        catch (Exception e){ e.printStackTrace(); }
    }

}
