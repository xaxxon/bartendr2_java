package bartendr;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

public class AppServlet extends HttpServlet {

    private ArduinoPort device;
    private String content = null;

    public AppServlet() {

        device = new ArduinoPort(this);
        printlog("bartendr2 app start (firmware v0.28)");

    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException { doPost(req,res); }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = null;
        try { action = request.getParameter("action");
        } catch (Exception e) {	e.printStackTrace(); }

        String val = null;
        try { val = request.getParameter("val");
        } catch (Exception e) {	e.printStackTrace(); }

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if (action == null) {
            out.println(content());
            return;
        }

        if (!action.equals("getinfo") && !action.equals("deviceoutput"))  printlog("action: "+action+" "+val);

        switch (action) {
            case "getinfo": out.println(getInfo()); break;

            case "reset": device.reset(); break;

            case "zero": device.sendCommand(ArduinoPort.ZERO); break;

            case "forceTarget":
                if (val == null) break;
                int g = (int) (Double.parseDouble(val) * 1000);
                if (g>65535) g= 65535;
                byte val1 = (byte) (g & 0xFF);
                byte val2 = (byte) ((g >>8) & 0xFF);
                device.sendCommand(new byte[]{ArduinoPort.FORCETARGET, val1, val2});
                break;

            case "stop": device.sendCommand(ArduinoPort.STOP); break;
            case "down": device.sendCommand(ArduinoPort.DOWN); break;
            case "home": device.sendCommand(ArduinoPort.HOME); break;
            case "up": device.sendCommand(ArduinoPort.UP); break;
            case "autoup": device.sendCommand(ArduinoPort.UPAUTOLOADHOLD); break;

            case "positionTarget":
                if (val == null) break;
                g = (int) (Double.parseDouble(val) * 1000);
                if (g>65535) g= 65535;
                val1 = (byte) (g & 0xFF);
                val2 = (byte) ((g >>8) & 0xFF);
                device.sendCommand(new byte[]{ArduinoPort.GOTOPOSITION, val1, val2});
                break;

            case "topheaterson": device.sendCommand(ArduinoPort.TOPHEATERSON); break;
            case "topheatersoff": device.sendCommand(ArduinoPort.TOPHEATERSOFF); break;
            case "backheaterson": device.sendCommand(ArduinoPort.BACKHEATERESON); break;
            case "backheatersoff": device.sendCommand(ArduinoPort.BACKHEATERSOFF); break;
            case "alloff": device.sendCommand(ArduinoPort.ALLOFF); break;

            case "ovenSetpoint":
                if (val == null) break;
                int c = Integer.parseInt(val);
                if (c>255) c= 255;
                device.sendCommand(new byte[]{ArduinoPort.SETPOINT, (byte) c});
                break;

            case "autotempon": device.sendCommand(ArduinoPort.AUTOTEMPON); break;
            case "autotempoff": device.sendCommand(ArduinoPort.AUTOTEMPOFF); break;

            case "convectfanon": device.sendCommand(ArduinoPort.CONVECTFANON); break;
            case "convectfanoff": device.sendCommand(ArduinoPort.CONVECTFANOFF); break;

            case "casefanon": device.sendCommand(ArduinoPort.CASEFANON); break;
            case "casefanoff": device.sendCommand(ArduinoPort.CASEFANOFF); break;

            case "deviceoutput":out.println(device.deviceoutput); break;

            case "shutdownhnow": shutdownhnow(); break;

            case "showlog": out.println(pukeLog()); break;

        }
    }


    private String getInfo() {
        String response = "";

        if (device.connected)     {
            response += "inrhtml('devicestatus','<b>connected</b>');";
            response += "setviz('resetlink','');";
        }
        else { // not connected
            response += "inrhtml('devicestatus','disconnected');";
            response += "setviz('resetlink','none');";
        }

        response += "inrhtml('currentForce','"+device.currentForce+" kg');";
        response += "inrhtml('forceTarget','"+device.forceTarget+" kg');";
        response += "inrhtml('actuatorPos','"+device.actuatorPos+" mm');";
        response += "inrhtml('actuatorDirection','"+device.actuatorDirection+"');";
        response += "inrhtml('positionTarget','"+device.positiontarget+" mm');";

        response += "inrhtml('ovenTemp','"+device.ovenTemp+" &deg;C');";
        response += "inrhtml('ovenSetpoint','"+device.ovenSetpoint+" &deg;C');";
        response += "inrhtml('topheaters','"+device.topheaters+"');";
        response += "inrhtml('backheaters','"+device.backheaters+"');";
        response += "inrhtml('barTemp','"+device.barTemp+" &deg;C');";
        response += "inrhtml('ambientTemp','"+device.ambientTemp+" &deg;C');";
        response += "inrhtml('cpuTemp','"+getCPUTemp()+" &deg;C');";

        response += "inrhtml('tempControlActive','"+device.tempControlActive+"');";

        response += "inrhtml('convectfan','"+device.convectfan+"');";
        response += "inrhtml('casefan','"+device.casefan+"');";

        return response;
    }

    private String content() {
//        if (content != null) return content; // TODO: enable after testing

        content = "";

        try {
            String line;
            FileInputStream filein = new FileInputStream("webapps/bartendr/content.html");
            BufferedReader reader = new BufferedReader(new InputStreamReader(filein));
            while ((line = reader.readLine()) != null)     content += line+"\n";
            reader.close();
            filein.close();
        } catch (Exception e) { e.printStackTrace(); }

        return content;
    }

    protected static void printlog(String str) {
        System.out.println(new Date().toString()+" "+str);
    }

    private int fahrenheitToCelsius(String temp) {
        double c = (Double.valueOf(temp) -32) / 1.8;
        return (int) Math.round(c);
    }

    private int convertToLbs(String psi) {
        double lbs = Double.valueOf(psi)*ArduinoPort.BARSQIN;
        return (int) Math.round(lbs);
    }

    private void shutdownhnow() {
        try { Runtime.getRuntime().exec("/usr/bin/sudo /sbin/shutdown -h now");
        } catch (Exception e) { e.printStackTrace(); }
    }

    private String pukeLog() {
        String str = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "<meta charset=\"UTF-8\">\n" +
                "<title>Bartendr</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<a href=\"/\">GO BACK</a><br><br>";

        try {
            String line;
            FileInputStream filein = new FileInputStream("logs/bartendr.log");
            BufferedReader reader = new BufferedReader(new InputStreamReader(filein));
            while ((line = reader.readLine()) != null)     str += line+"<br>";
            reader.close();
            filein.close();
        } catch (Exception e) { e.printStackTrace(); }

        str += "</body></html>";

        return str;
    }

    public static String readUrlToString(String urlString) {
        try {
            URL website = new URL(urlString);
            URLConnection connection = website.openConnection();
            BufferedReader in = new BufferedReader( new InputStreamReader( connection.getInputStream()));

            StringBuilder response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null) response.append(inputLine);
            in.close();
            return response.toString();

        } catch (Exception e) {
//			Util.debug("Util.readUrlToString() parse error: "+urlString, "Util.readUrlToString()");
            return null;
        }
    }

    private static double getCPUTemp() {
        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("/sys/class/thermal/thermal_zone0/temp")));
            String line = reader.readLine();
            reader.close();
            double temp = Double.valueOf(line.strip())/1000;
            return temp;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0.0;
    }
}
